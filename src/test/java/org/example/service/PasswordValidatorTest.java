package org.example.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
class PasswordValidatorTest {

    private PasswordValidator passwordValidator;

    @BeforeEach
    void setUp() {
        passwordValidator = new PasswordValidator();
    }

    @Test
    void testPasswordIsValid() {
        assertFalse(passwordValidator.isNotValid("ABcd!@12"));
    }

    @Test
    void testMissingNumber() {
        assertTrue(passwordValidator.isNotValid("M#OIes"));
    }

    @Test
    void testMissingLowercase() {
        assertTrue(passwordValidator.isNotValid("PWT%23"));
    }

    @Test
    void testMissingUppercase() {
        assertTrue(passwordValidator.isNotValid("anb5uo^"));
    }

    @Test
    void testMissingSpecialChar() {
        assertTrue(passwordValidator.isNotValid("Asf4"));
    }

    @Test
    void testToShort() {
        assertTrue(passwordValidator.isNotValid("Q!2"));
    }

    @Test
    void testTooLong() {
        assertTrue(passwordValidator.isNotValid("hwy284h2kdi92uUej29"));
    }

    @Test
    void testEmptyPass() {
        assertTrue(passwordValidator.isNotValid(""));
    }

    @Test
    void testPassIsNull() {
        assertTrue(passwordValidator.isNotValid(null));
    }
}