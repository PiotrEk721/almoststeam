package org.example.service;

import org.example.MainUser;
import org.junit.jupiter.api.Test;

import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class UserServiceTest {

    private UserService userService;
    private PasswordValidator passwordValidator;
    private Scanner scanner;

    @Test
    void testLoginSuccess() {
// given
        when(scanner.nextLine()).thenReturn("existingUser", "correctPassword");
        when(passwordValidator.isNotValid(anyString())).thenReturn(false);

//when
        User existUser = new User("existUser", "correctPassword");
        userService.users.put("existingUser", existUser);
        MainUser mainUser = userService.login(scanner);
//then
        assertNotNull(mainUser);
        assertEquals("existingUser", mainUser.getUsername());

    }
}