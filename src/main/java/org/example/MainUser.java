package org.example;


import org.example.service.App;
import org.example.service.UserService;
import org.example.service.User;
import org.example.entity.Wallet;

import java.util.Scanner;

public class MainUser extends User {
    public static void main(String[] args) {
        welcomeMessage();

        startApp();
    }

    public static void startApp() {
        UserService userService = new UserService();
        App app = new App();

        String choice;
        do {
            printSelection();
            choice = in.nextLine();
            if (checkTheReceivedCharacter(choice, CORRECT_CHOICE_CHARACTERS)) {
                switch (choice) {
                    case "1":
                        userService.register(in);
                        break;
                    case "2":
                        MainUser currentUser = userService.login(in);
                        if (currentUser != null) {
                            app.userMenu(currentUser, in);
                        }
                        break;
                    case "3":
                        System.out.println("Closing the application.");
                        System.exit(0);
                        break;
                    default:
                        System.out.println("Invalid choice. Please select a valid option.");
                        break;
                }
            } else {
                printMessageAfterIncorrectChoice(CORRECT_CHOICE_CHARACTERS);
            }
        } while (!checkTheReceivedCharacter(choice, CORRECT_CHOICE_CHARACTERS));
    }

    public static void welcomeMessage() {
        System.out.println("\u001B[31m\u001B[1m"  // Set red color and bold style
                .concat("[ Welcome in AlmostSteam ]\n")
                .concat("[ App to interact with no one, with poor game library ]")
                .concat("\u001B[0m"));
    }

    public static void printSelection() {
        System.out.println("1. Register\n"
                .concat("2. Login\n")
                .concat("3. Close\n")
                .concat("Select an option: "));
    }

    public MainUser(String username) {
        super(username, null);
    }

    static final char[] CORRECT_CHOICE_CHARACTERS = {'1', '2', '3'};
    static final int sizeCorrectChoiceCharacter = CORRECT_CHOICE_CHARACTERS.length;
    private Wallet userWallet;
    static Scanner in = new Scanner(System.in);

    public void setUserWallet(Wallet userWallet) {
        this.userWallet = userWallet;
    }

    public Wallet getUserWallet() {
        return userWallet;
    }

    public static void printMessageAfterIncorrectChoice(char[] chars) {
        System.out.print("Incorrect choice! Please enter one of the listed characters: {");
        for (int i = 0; i < chars.length; i++) {
            if (i <= chars.length - 2) {
                System.out.printf("%c, ", chars[i]);
            } else {
                System.out.printf("%c}\n\n", chars[sizeCorrectChoiceCharacter - 1]);
            }
        }
    }

    public static boolean checkTheReceivedCharacter(String string, char... chars) {
        for (char characters : chars) {
            if (getCharFromString(string) == characters) {
                return true;
            }
        }
        return false;
    }

    public static char getCharFromString(String string) {
        return string.charAt(0);
    }
}

