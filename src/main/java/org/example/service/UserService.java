package org.example.service;

import org.example.MainUser;
import org.example.entity.Wallet;
import org.example.fileIO.ConstantFilePaths;
import org.example.fileIO.FilesName;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static org.example.fileIO.ConstantFilePaths.SEP;

public class UserService {
    static final String USERS_LIST_FILE = ConstantFilePaths.FILE_PATH_USERS_LIST.concat(SEP).concat(FilesName.USERS_LIST_FILE_NAME);
    public final Map<String, User> users = new HashMap<>();
    private final PasswordValidator passwordValidator;
    public User currentUser;

    public void setCurrentUser(User user) {
        this.currentUser = user;
    }

    public UserService() {
        this.passwordValidator = new PasswordValidator();
        if (Files.exists(Paths.get(USERS_LIST_FILE))) {
            loadUsers();
        } else {
            try {
                Files.createFile(Paths.get(USERS_LIST_FILE));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void loadUsers() {
        try (BufferedReader br = new BufferedReader(new FileReader(USERS_LIST_FILE))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split(",");
                users.put(parts[0], new User(parts[0], parts[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveUsers() {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(USERS_LIST_FILE))) {
            for (User user : users.values()) {
                bw.write(user.getUsername() + "," + user.getPassword());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public MainUser login(Scanner in) {
        int MAX_FAILED_ATTEMPTS = 3;
        while (true) {
            System.out.print("Enter your username: ");
            String username = in.nextLine();
            System.out.print("Enter your password: ");
            String password = in.nextLine();

            if (users.containsKey(username)) {
                User user = users.get(username);

                if (user.getFailedLoginAttempts() >= MAX_FAILED_ATTEMPTS) {
                    System.out.println("Account is blocked due to too many failed login attempts.");
                    System.exit(0);
                }

                if (user.getPassword().equals(password)) {
                    System.out.println("\u001B[32m" + "\n\nLogin successful. Welcome, " + username + "!\u001B[0m\n");
                    user.resetFailedLoginAttempts();
                    saveUsers();
                    setCurrentUser(user);
                    Wallet userWallet = user.getWallet();
                    userWallet.loadWalletState(username);
                    return new MainUser(username);
                } else {
                    System.out.println("Incorrect password.");
                    user.incrementFailedLoginAttempts();
                    saveUsers();
                }
            } else {
                System.out.println("Account doesn't exist. Go Away!");
            }
            System.out.println("Would you like to try again? (y/n): ");
            String tryAgain = in.nextLine().trim().toLowerCase();

            if ("n".equals(tryAgain)) {
                System.out.println("Exiting...");
                System.exit(0);
            }
        }
    }


    public void register(Scanner in) {
        App menu = new App();
        MainUser user = new MainUser("in");
        int failedPasswordAttempts = 0;
        String username;

        while (true) {
            System.out.print("Enter a new username: ");
            username = in.nextLine();
            if (!users.containsKey(username)) {
                break;
            } else {
                System.out.println("The username is already taken. Please choose another one.");
            }
        }

        while (true) {

            System.out.print("Enter password, password must contain: \n" +
                    " - 4-16 characters\n" +
                    " - At least 1x uppercase letter\n" +
                    " - At least 1x lowercase letter\n" +
                    " - At least 1x digit\n" +
                    " - At least 1x special character: ");
            String password = in.nextLine();

            if (passwordValidator.isNotValid(password)) {
                System.out.println("\n\u001B[31m\u001B[1m"+"[ Invalid password format.] "+"\u001B[0m");
                failedPasswordAttempts++;
                if (failedPasswordAttempts >= 3) {
                    System.out.println("\u001B[31m\u001B[1m"+"[ Too many failed attempts. Go Away!!]"+"\u001B[0m");
                    System.exit(0);
                }
            } else {
                users.put(username, new User(username, password));
                System.out.println("\n\n\u001B[32m" + "Successful registration." + "\u001B[0m\n");
                saveUsers();
                menu.userMenu(user, in);
                break;
            }
        }
    }

    public void changePassword(MainUser mainUser, Scanner in) {
        if (mainUser == null || !users.containsKey(mainUser.getUsername())) {
            System.out.println("User not found!");
            return;
        }

        System.out.print("Enter your current password: ");
        String currentPassword = in.nextLine();

        String username = mainUser.getUsername();
        User user = users.get(username);

        if (!user.getPassword().equals(currentPassword)) {
            System.out.println("Incorrect current password.");
            return;
        }

        System.out.print("Enter your new password: ");
        String newPassword = in.nextLine();

        if (passwordValidator.isNotValid(newPassword)) {
            System.out.println("Invalid password format. " +
                    "Password should have at least 4 characters," +
                    " with one uppercase letter," +
                    " one lowercase letter," +
                    " one digit," +
                    " and one special character.");
            return;
        }

        user.setPassword(newPassword);
        System.out.println("Password successfully changed.");
        saveUsers();
    }

    public void deleteUserAccount(MainUser user, Scanner in) {
        if (user == null || !users.containsKey(user.getUsername())) {
            System.out.println("User not found!");
            return;
        }

        System.out.print("Are you sure you want to delete your account? (Y/N): ");
        String confirm = in.nextLine();

        if ("Y".equalsIgnoreCase(confirm)) {
            if (users.remove(user.getUsername()) != null) {
                System.out.println("\u001B[31m\u001B[1m[ Account Delete... ]\u001B[0m");
                System.out.println("\u001B[31m\u001B[1m[ BUT, WE KNOW YOU WILL BACK ;P ]\u001B[0m");
            } else {
                System.out.println("Account not found.");
            }
            saveUsers();
            MainUser.startApp();
        } else {
            System.out.println("Operation cancelled.");
        }
    }
}




