package org.example.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {
    // regex = 1x digit, 1x lowercase letter, 1 uppercase letter, 1x special character, 4-16 digits - no less no more
    public boolean isNotValid(String password) {
        String pattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*@#$%^&+=!_\\-]).{4,16}$";
        Pattern regex = Pattern.compile(pattern);
        Matcher matcher = regex.matcher(password);
        return !matcher.matches();
    }
}