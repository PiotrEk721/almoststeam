package org.example.service;

import org.example.entity.UserGameLibrary;
import org.example.entity.Wallet;

public class User {
    private final String username;
    private String password;
    private int failedLoginAttempts;
    private final UserGameLibrary gameLibrary;

    private Wallet wallet;

    public Wallet getWallet() {
        return wallet;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.failedLoginAttempts = 0;
        this.gameLibrary = new UserGameLibrary();
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getFailedLoginAttempts() {
        return failedLoginAttempts;
    }

    public void incrementFailedLoginAttempts() {
        this.failedLoginAttempts++;
    }

    public void resetFailedLoginAttempts() {
        this.failedLoginAttempts = 0;
    }
}
