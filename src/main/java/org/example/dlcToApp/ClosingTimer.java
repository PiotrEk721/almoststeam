package org.example.dlcToApp;

import java.util.Timer;
import java.util.TimerTask;

public class ClosingTimer {
    private final Timer timer;
    private int remainingTime;

    public ClosingTimer(int countdownInSeconds) {
        this.remainingTime = countdownInSeconds;
        this.timer = new Timer();
    }

    public void startClosing() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (remainingTime >= 0) {
                    System.out.println("Don't Leave Us!!! : " + remainingTime);
                    remainingTime--;
                } else {
                    System.out.println("\u001B[31m\u001B[1m[ WE KNOW YOU WILL BACK ;P ]\u001B[0m");
                    timer.cancel();
                    System.exit(0);
                }
            }
        },0, 1000);
    }
}
