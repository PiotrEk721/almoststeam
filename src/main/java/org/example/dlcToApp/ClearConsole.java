package org.example.dlcToApp;
// Maybe someday it will work.
public class ClearConsole {
    public static void clearConsole() {
        final String os = System.getProperty("os.name");
        if (os.contains("Windows")) {
            clearWindowsConsole();
        } else {
            clearUnixConsole();
        }
    }

    public static void clearWindowsConsole() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void clearUnixConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
