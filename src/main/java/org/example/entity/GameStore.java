package org.example.entity;

import org.example.MainUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class GameStore {


    private final List<Game> gameList = new ArrayList<>();

    public GameStore() {
        initStorage();
    }

    public List<Game> getGameList() {
        return gameList;
    }

    private void initStorage() {
        this.gameList.add(new Game(1,
                "Dark Souls III",
                GameCategory.SOULSLIKE,
                "FromSoftware",
                199.99));
        this.gameList.add(new Game(2,
                "The Witcher 3: Wild Hunt",
                GameCategory.RPG,
                "CD Projekt",
                99.99));
        this.gameList.add(new Game(3,
                "Guild Wars 2",
                GameCategory.MMORPG,
                "NCSOFT",
                0.01));
        this.gameList.add(new Game(4,
                "Devil May Cry 5",
                GameCategory.ACTON,
                "Capcom",
                120.00));
        this.gameList.add(new Game(5,
                "Bloodborne",
                GameCategory.SOULSLIKE,
                "FromSoftware",
                89.90));
        this.gameList.add(new Game(6,
                "Sekiro: Shadows Die Twice",
                GameCategory.SOULSLIKE,
                "FromSoftware",
                254.00));
        this.gameList.add(new Game(7,
                "Nioh",
                GameCategory.SOULSLIKE,
                "Koei Tecmo",
                231.99));
        this.gameList.add(new Game(8,
                "Ashen",
                GameCategory.SOULSLIKE,
                "A44",
                217.80));
        this.gameList.add(new Game(9,
                "Divinity: Original Sin 2",
                GameCategory.RPG,
                "Larian Studios",
                161.99));
        this.gameList.add(new Game(10,
                "Dragon Age: Inquisition",
                GameCategory.RPG,
                "BioWare",
                179.90));
        this.gameList.add(new Game(11,
                "Pillars of Eternity",
                GameCategory.RPG,
                "Paradox interactive",
                107.99));
        this.gameList.add(new Game(12,
                "World of Warcraft",
                GameCategory.MMORPG,
                "Blizzard Entertainment",
                147.77));
        this.gameList.add(new Game(13,
                "Final Fantasy XIV",
                GameCategory.MMORPG,
                "Square Enix",
                146.99));
        this.gameList.add(new Game(14,
                "The Elder Scrolls Online",
                GameCategory.MMORPG,
                "ZeniMax Online Studios",
                79.90));
        this.gameList.add(new Game(15,
                "Black Desert Online",
                GameCategory.MMORPG,
                "Pearl Abyss",
                45.00));
        this.gameList.add(new Game(16,
                "Grand Theft Auto V",
                GameCategory.ACTON,
                "Rockstar Games",
                129.60));
        this.gameList.add(new Game(17,
                "Metal Gear Solid V",
                GameCategory.ACTON,
                "Konami",
                73.00));
        this.gameList.add(new Game(18,
                "Bayonetta",
                GameCategory.ACTON,
                "Sega",
                239.00));
        this.gameList.add(new Game(19,
                "Doom",
                GameCategory.ACTON,
                "Bethesda",
                79.99));
        this.gameList.add(new Game(20,
                "Dark Souls",
                GameCategory.SOULSLIKE,
                "FromSoftware",
                149.90));
        this.gameList.add(new Game(21,
                "Cities: Skylines",
                GameCategory.SYMULATION,
                "Paradox Interactive",
                138.99));
        this.gameList.add(new Game(22,
                "Imperator: Rome",
                GameCategory.STRATEGY,
                "Paradox Interactive",
                254.99));
        this.gameList.add(new Game(23,
                "Europa Universalis IV",
                GameCategory.STRATEGY,
                "Paradox Interactive",
                184.99));
        this.gameList.add(new Game(24,
                "Anno 1800",
                GameCategory.STRATEGY,
                "Ubisoft",
                249.90));
        this.gameList.add(new Game(25,
                "Timberborn",
                GameCategory.STRATEGY,
                "Mechanistry",
                114.99));
        this.gameList.add(new Game(26,
                "Satisfactory",
                GameCategory.SYMULATION,
                "Coffee Stain Publishing",
                138.99));
        this.gameList.add(new Game(27,
                "Goat Simulator",
                GameCategory.SYMULATION,
                "Coffee Stain Publishing",
                35.99));
        this.gameList.add(new Game(28,
                "Euro Truck Simulator 2",
                GameCategory.SYMULATION,
                "SCS Software",
                79.99));
        this.gameList.add(new Game(29,
                "Construction Simulator",
                GameCategory.SYMULATION,
                "astragon Entertainment",
                124.99));
        this.gameList.add(new Game(30,
                "Car Mechanic Simulator 2021",
                GameCategory.SYMULATION,
                "PlayWay S.A.",
                114.99));
        this.gameList.add(new Game(31,
                "GRID",
                GameCategory.RACING,
                "Codemasters, Electronic Arts",
                107.99));
        this.gameList.add(new Game(32,
                "Forza Horizon 5",
                GameCategory.RACING,
                "Xbox Game Studios",
                249.99));
        this.gameList.add(new Game(33,
                "Assetto Corsa",
                GameCategory.RACING,
                "Kunos Simulazioni",
                71.99));
        this.gameList.add(new Game(34,
                "SimRail - The Railway Simulator",
                GameCategory.SYMULATION,
                "PlayWay S.A.",
                149.99));
        this.gameList.add(new Game(35,
                "Teardown",
                GameCategory.SYMULATION,
                "Tuxedo Labs",
                119.00));
        this.gameList.add(new Game(36,
                "Space Engineers",
                GameCategory.SYMULATION,
                "Keen Software House",
                71.99));
        this.gameList.add(new Game(37,
                "RimWorld",
                GameCategory.SYMULATION,
                "Ludeon Studios",
                139.99));
        this.gameList.add(new Game(38,
                "Factorio",
                GameCategory.STRATEGY,
                "Wube Software LTD.",
                160.00));
        this.gameList.add(new Game(39,
                "Techtonica",
                GameCategory.SYMULATION,
                "Fire Hose Games",
                138.99));
        this.gameList.add(new Game(40,
                "Heroes® of Might & Magic® III - HD Edition",
                GameCategory.STRATEGY,
                "Ubisoft Entertainment",
                59.90));
        this.gameList.add(new Game(41,
                "The Elder Scrolls V Skyrim Special Edition",
                GameCategory.RPG,
                "Bethesda Softworks",
                169.00));
        this.gameList.add(new Game(42,
                "Total War: WARHAMMER III",
                GameCategory.STRATEGY,
                "SEGA, Feral Interactive",
                219.99));
        this.gameList.add(new Game(43,
                "HUMANKIND™",
                GameCategory.STRATEGY,
                "SEGA",
                199.00));
        this.gameList.add(new Game(44,
                "Farthest Frontier",
                GameCategory.STRATEGY,
                "Crate Entertainment",
                107.99));
        this.gameList.add(new Game(45,
                "The Settlers® 7 : History Edition",
                GameCategory.STRATEGY,
                "Ubisoft",
                59.90));
    }

    @Override
    public String toString() {
        return "GameList{" +
                gameList + '}';
    }

    public Game getGameById(int id) {
        for (Game game : gameList) {
            if (game.getId() == id) {
                return game;
            }
        }
        return null;
    }

    public Optional<Game> buyGame(int id, Wallet userWallet) {
        Optional<Game> optionalGame = gameList.stream().filter(game -> game.getId() == id).findFirst();
        if (optionalGame.isPresent()) {
            Game game = optionalGame.get();
            if (userWallet.getBalance() >= game.getPrice()) {

                // Additional finanse operation
                Publisher.addRevenue(game.getPublisher(), game.getPrice() * 0.35);
                SteamWallet.addBalance(game.getPrice() * 0.65);
                return Optional.of(game);
            }
        }
        return Optional.empty();
    } // deduct method needed

    public void showGameMenu(int gameId, Scanner in, MainUser user) {
        GameStore gameStore = new GameStore();
        Game selectedGame = getGameById(gameId);
        if (selectedGame == null) {
            System.out.println("Game Not Found.");
            return;
        }
        System.out.println(selectedGame);

        System.out.println("\n1. Buy this game.\n"
                .concat("2. Back to game list.\n")
                .concat("3. Back to main menu"));

        int choice = in.nextInt();
        in.nextLine();

        switch (choice) {
            case 1:
                Wallet userWallet = user.getUserWallet();
                Optional<Game> purchasedGame = buyGame(gameId, userWallet);
                if (purchasedGame.isPresent()) {
                    UserGameLibrary.getUserGameLibrary().add(purchasedGame.get());
                    System.out.println("Game Purchase Successfully");
                } else {
                    System.out.println("Add some cash for games :D");
                }
                break;
            case 2:
                for (Game game : gameStore.getGameList()) {
                    System.out.println(game.getId() +
                            ". \u001B[32m" + game.getTitle() +
                            "\u001B[0m - Price: " +
                            String.format("%.2f", game.getPrice()));
                }
                System.out.println("\n\nChoose game ID or 'X' to go back to the main menu.");
                String gameChoice = in.nextLine().trim();
                if ("X".equalsIgnoreCase(gameChoice)) {
                    break;
                } else {
                    try {
                        int gameChoiceInt = Integer.parseInt(gameChoice);
                        gameStore.showGameMenu(gameChoiceInt, in, user);
                    } catch (NumberFormatException e) {
                        System.out.println("\n\nInvalid input, please enter a valid game ID or 'X' to exit.\n\n");
                    }
                }
                break;
            case 3:
                break;
            default:
                System.out.println("Invalid Choice");
        }
    }
}
