package org.example.entity;

public class SteamWallet {
    static double balance = 0;

    public static void addBalance(double amount){
        balance += amount;
    }
    public static double getBalance() {
        return balance;
    }
}
