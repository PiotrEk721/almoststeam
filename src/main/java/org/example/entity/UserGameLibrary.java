package org.example.entity;

import org.example.service.User;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class UserGameLibrary {
    private static  final ArrayList<Game> library = new ArrayList<>();
    public static ArrayList<Game> getUserGameLibrary() {
        return library;
    }

public static void showLibrary(User currentUser) {
    System.out.println("Your game library: ");
    for(Game game: library) {
        System.out.println(game.toString());
    }
}
public static void addGame(Game game){
        library.add(game);
}
    public void saveLibraryToFile(){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("gameLibrary.txt"))){
            for (Game game: library) {
                writer.write(game.toString());
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
