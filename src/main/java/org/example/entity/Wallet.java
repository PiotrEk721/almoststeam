package org.example.entity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Wallet {
    private static double balance;
    private static Wallet instance;
    public  String username;

    public Wallet(String username) {
        balance = 0.0;
        this.username = username;
    }

    public static Wallet getWallet(String username) {
        if (instance == null) {
            instance = new Wallet(username);
        }
        return instance;
    }

    public static void saveWalletState(String username) {
        try {
            FileWriter writer = new FileWriter(username + "_wallet.txt");
            writer.write(Double.toString(balance));
            writer.close();
        } catch (IOException e) {
            System.out.println("\nError while saving the wallet state.\n");
        }
    }

    public static void loadWalletState(String username) {
        try {
            File file = new File(username + "_wallet.txt");
            Scanner reader = new Scanner(file);
            if (reader.hasNextLine()) {
                balance = Double.parseDouble(reader.nextLine());
            }
            reader.close();
        } catch (FileNotFoundException e) {
           // System.out.println("Wallet account not fount. You did something wrong, now your account balance is 0");
            balance = 0.0;
        }
    }
    public static double checkBalance() {
        return balance;
    }

    public double getBalance() {
        return balance;
    }

    public static void deposit(double amount, String username) {
        balance += amount;
        saveWalletState(username);
    }

    public static boolean deduct(double amount, String username) {
        if (amount > balance) {
            return false;
        }
        balance -= amount;
        saveWalletState(username);
        return true;
        
        //add cash when balance is too low to but game
    }

    public void walletMenu(Scanner in, String username) {
        System.out.println("\u001B[32m" + "Here is your wallet, add funds to your account to purchase any of our products" + "\u001B[0m");
        while (true) {
            System.out.println("\nChoose one of the options below:");
            System.out.println("1: Account balance");
            System.out.println("2: Add funds");
            System.out.println("3: Close Wallet");
            int choice = in.nextInt();

            switch (choice) {
                case 1:
                    if (balance == 0.0) {
                        System.out.println("\nYour account is empty. Please add some cash.");
                    } else {
                        System.out.println("\nYour current balance = " + getBalance());
                    }
                    break;
                case 2:
                    System.out.println("Available denominations: 10, 20, 50, 100, 400");
                    System.out.println("How much you want to add: ");
                    double depositAmount = in.nextDouble();
                    if (depositAmount == 10 || depositAmount == 20 || depositAmount == 50 || depositAmount == 100 || depositAmount == 400) {
                        deposit(depositAmount, username);
                        System.out.println("Added " + depositAmount + " to your wallet.\n");
                    } else {
                        System.out.println("Incorrectly entered amount\n");
                    }
                    walletMenu(in, username);
                case 3:
                    System.out.println("Closing Wallet, here is your Menu");
                    return;
                default:
                    System.out.println("Wrong choice.");
                    break;
            }
        }
    }
}
