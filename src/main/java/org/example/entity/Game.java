package org.example.entity;

public class Game {

    int id;
    String title;
    GameCategory category;
    String publisher;
    double price;


    public Game(int id, String title, GameCategory category, String publisher, double price) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.publisher = publisher;
        this.price = price;

    }

    public int getId() {
        return id;
    }

    public double getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getPublisher() {
        return publisher;
    }

    @Override
    public String toString() {
        return "\n\u001B[1m\u001B[32m" + title + "\u001B[0m\n" +
                "Category: " + category + "\n" +
                "Publisher: " + publisher + "\n" +
                "Price: " + String.format("%.2f", price);
    }

}
