package org.example.fileIO;

import org.example.entity.Game;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import static org.example.fileIO.ConstantFilePaths.SEP;

public class FileInputStreamToBase {
	FileCreator fileCreator = new FileCreator();
	public void gameStorageStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_GAME_STORAGE_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_GAME_STORAGE_DATA
						.concat(SEP).concat(FilesName.GAME_STORAGE_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_GAME_STORAGE_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_GAME_STORAGE_DATA
					.concat(SEP).concat(FilesName.GAME_STORAGE_ARCHIVE_FILE_NAME));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void userGameStorageStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_USER_GAME_STORAGE_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_USER_GAME_STORAGE_DATA
						.concat(SEP).concat(FilesName.USER_GAME_STORAGE_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_USER_GAME_STORAGE_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_USER_GAME_STORAGE_DATA
					.concat(SEP).concat(FilesName.USER_GAME_STORAGE_ARCHIVE_FILE_NAME));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void walletUserStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_USER_WALLET_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_USER_WALLET_DATA
						.concat(SEP).concat(FilesName.USER_WALLET_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_USER_WALLET_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_USER_WALLET_DATA
					.concat(SEP).concat(FilesName.USER_WALLET_ARCHIVE_FILE_NAME));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void walletSteamStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_STEAM_WALLET_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_STEAM_WALLET_DATA
						.concat(SEP).concat(FilesName.STEAM_WALLET_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_STEAM_WALLET_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_STEAM_WALLET_DATA
					.concat(SEP).concat(FilesName.STEAM_WALLET_ARCHIVE_FILE_NAME));

			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void walletPublisherStatus() {
		fileCreator.createFileIfNotExist(ConstantFilePaths.FILE_PATH_PUBLISHER_WALLET_DATA);
		try (
				FileInputStream in = new FileInputStream(ConstantFilePaths.FILE_PATH_PUBLISHER_WALLET_DATA
						.concat(SEP).concat(FilesName.PUBLISHER_WALLET_FILE_NAME))
		) {
			fileCreator.createFileIfNotExist(ConstantFilePaths.ARCHIVE_FILE_PATH_PUBLISHER_WALLET_DATA);
			FileOutputStream out = new FileOutputStream(ConstantFilePaths.ARCHIVE_FILE_PATH_PUBLISHER_WALLET_DATA
					.concat(SEP).concat(FilesName.PUBLISHER_WALLET_ARCHIVE_FILE_NAME));
			int c;
			while ((c = in.read()) != -1) {
				out.write(c);
			}
			out.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void saveDataInBase(List<Game> games, String filePath) {
		/* dodać wskazanie na konkretny plik */
		try {
			String text = games.toString();
			Files.writeString(Paths.get(filePath + SEP + "GameStorage.txt"), text, StandardOpenOption.CREATE);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	public void printDate(String filePath) {
		/* dodać wskazanie na konkretny plik */
		try {
			List<String> file = Files.readAllLines(Paths.get(filePath));
			file.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
