package org.example.fileIO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderFromBase {
	public void showFileContents(String pathFile, String message) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(pathFile));

			String line;
			printMessage(message);

			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}
			reader.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void printMessage(String message) {
		System.out.println(message);
	}
}