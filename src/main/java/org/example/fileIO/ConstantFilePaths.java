package org.example.fileIO;

import java.io.File;

public class ConstantFilePaths {

	public static final String SEP = File.separator;

	public static final String FILE_PATH_RESOURCES = "src" + SEP + "main" + SEP + "resources";

	public static final String FILE_PATH_GAME_STORAGE_DATA = FILE_PATH_RESOURCES + SEP + "game" + SEP + "gameStorage";
	protected static final String ARCHIVE_FILE_PATH_GAME_STORAGE_DATA = FILE_PATH_GAME_STORAGE_DATA + SEP + "archiv_gameStorage";

	public static final String FILE_PATH_USER_GAME_STORAGE_DATA = FILE_PATH_RESOURCES + SEP + "game" + SEP + "userGameStorage";
	protected static final String ARCHIVE_FILE_PATH_USER_GAME_STORAGE_DATA = FILE_PATH_USER_GAME_STORAGE_DATA + SEP + "archiv_userGameStorage";

	public static final String FILE_PATH_USER_WALLET_DATA = FILE_PATH_RESOURCES + SEP + "wallet" + SEP + "user_wallet";
	protected static final String ARCHIVE_FILE_PATH_USER_WALLET_DATA = FILE_PATH_USER_WALLET_DATA + SEP + "archiv_userWallet";

	public static final String FILE_PATH_STEAM_WALLET_DATA = FILE_PATH_RESOURCES + SEP + "wallet" + SEP + "steam_wallet";
	protected static final String ARCHIVE_FILE_PATH_STEAM_WALLET_DATA = FILE_PATH_STEAM_WALLET_DATA + SEP + "archiv_steamWallet";

	public static final String FILE_PATH_PUBLISHER_WALLET_DATA = FILE_PATH_RESOURCES + SEP + "wallet" + SEP + "publisher_wallet";
	protected static final String ARCHIVE_FILE_PATH_PUBLISHER_WALLET_DATA = FILE_PATH_PUBLISHER_WALLET_DATA + SEP + "archiv_publisherWallet";

	public static final String FILE_PATH_ADMIN_DATA = FILE_PATH_RESOURCES + SEP + "admin";
	public static final String FILE_PATH_USERS_LIST = FILE_PATH_RESOURCES + SEP + "users";
}
