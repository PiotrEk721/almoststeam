package org.example.fileIO;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FilesName {
	static Date date = new Date();
	private static final String DATA_FORMAT = "yyyyMMdd_HHmmss";
	protected static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATA_FORMAT);
	public static final String FILE_EXTENSION = ".txt";

	public static final String nameOfUserWallet = "UserWallet";
	public static final String USER_WALLET_FILE_NAME = nameOfUserWallet.concat(FILE_EXTENSION);
	public static final String USER_WALLET_ARCHIVE_FILE_NAME = nameOfUserWallet.concat(simpleDateFormat.format(date)).concat(FILE_EXTENSION);

	public static final String nameOfPublisherWallet = "PublisherWallet";
	public static final String PUBLISHER_WALLET_FILE_NAME = nameOfPublisherWallet.concat(FILE_EXTENSION);
	public static final String PUBLISHER_WALLET_ARCHIVE_FILE_NAME = nameOfPublisherWallet.concat(simpleDateFormat.format(date)).concat(FILE_EXTENSION);

	public static final String nameOfSteamWallet = "SteamWallet";
	public static final String STEAM_WALLET_FILE_NAME = nameOfSteamWallet.concat(FILE_EXTENSION);
	public static final String STEAM_WALLET_ARCHIVE_FILE_NAME = nameOfSteamWallet.concat(simpleDateFormat.format(date)).concat(FILE_EXTENSION);

	public static final String nameOfGameStorage = "GameStorage";
	public static final String GAME_STORAGE_FILE_NAME = nameOfGameStorage.concat(FILE_EXTENSION);
	public static final String GAME_STORAGE_ARCHIVE_FILE_NAME = nameOfGameStorage.concat(simpleDateFormat.format(date)).concat(FILE_EXTENSION);

	public static final String nameOfUserGameStorage = "UserGameStorage";
	public static final String USER_GAME_STORAGE_FILE_NAME = nameOfUserGameStorage.concat(FILE_EXTENSION);
	public static final String USER_GAME_STORAGE_ARCHIVE_FILE_NAME = nameOfUserGameStorage.concat(simpleDateFormat.format(date)).concat(FILE_EXTENSION);

	public static final String nameOfAdminFile = "admin_credentials";
	public static final String ADMIN_FILE_NAME = nameOfAdminFile.concat(FILE_EXTENSION);

	public static final String nameOfUserListFile = "users";
	public static final String USERS_LIST_FILE_NAME = nameOfUserListFile.concat(FILE_EXTENSION);

}
